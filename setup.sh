#!/bin/bash
# shell script that installs a rabbitmq server onto your local linux system
# and gets this little hack up and running as a development project
# 
# NOTE: sudo access is required for this to work
# 
#from http://www.tecmint.com/how-to-enable-epel-repository-for-rhel-centos-6-5/
wget http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm
rpm -ivh epel-release-7-5.noarch.rpm

#from https://chriscowley.me.uk/blog/2014/11/18/installing-rabbitmq-on-centos-7/
yum -y install rabbitmq-server

firewall-cmd --permanent --add-port=5672/tcp
# firewalld wasn't running, but your mileage may vary

setsebool -P nis_enabled 1
# disabled selinux

systemctl enable rabbitmq-server
systemctl start rabbitmq-server

sudo rabbitmqctl status
# shows that it's working

#from https://www.rabbitmq.com/install-rpm.html
ulimit -n
# showed the bare minimum of 1024 on my system; you will want to up this number

ulimit -S -n 4096

# for development, you'll likely need to install php's bcmath module
yum --enablerepo=remi-php56 install php-bcmath


