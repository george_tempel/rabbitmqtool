<?php

// include the lib(s) and necessary classes

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

$options = getopt('q:s:p:u:w:in:xd',
        array(
                'queue:',
                'server:',
                'port:',
                'username:',
                'password:',
        )
);
$queueName = array_key_exists('q', $options) ? $options['q'] : (array_key_exists('queue', $options) ? $options['queue'] : 'hello');
$queueServer = array_key_exists('s', $options) ? $options['s'] : (array_key_exists('server', $options) ? $options['server'] : 'localhost');
$queuePort = array_key_exists('p', $options) ? $options['p'] : (array_key_exists('port', $options) ? $options['port'] : 5672);
$username = array_key_exists('u', $options) ? $options['u'] : (array_key_exists('username', $options) ? $options['username'] : 'guest');
$password = array_key_exists('w', $options) ? $options['w'] : (array_key_exists('password', $options) ? $options['password'] : 'guest');
$input = array_key_exists('i', $options) || array_key_exists('input', $options);
$numberOfMessages = array_key_exists('n', $options) ? abs(1 * $options['n']) : 1;
$numberOfMessages = ($numberOfMessages > 0) ? $numberOfMessages : 1;
$useExchange = array_key_exists('x', $options);
$useDeadLetter = array_key_exists('d', $options);

// create the connection to the server

$connection = new AMQPConnection($queueServer, $queuePort, $username, $password);

$channel = $connection->channel();

$exchangeName = $queueName . '.exchange';
$deadLetterQueueName = $queueName . '.deadLetter.queue';
$deadLetterExchangeName = $queueName . '.deadLetter.exchange';
$args = $useDeadLetter ? [
	'x-dead-letter-exchange' => ['S', $deadLetterExchangeName],
	//'x-dead-letter-routing-key' => ['S', $queueName . '.deadLetter.routingKey' ],
] : [];

$channel->queue_declare($queueName,
$passive = false,
$durable = true,
$exclusive = false,
$autoDelete = false,
$noWait = false,
$args, // we want to specify the dlx here
$ticket = null);

if ($useExchange) {
	$channel->exchange_declare($exchangeName,
		$type = 'direct',
		$passive = false,
		$durable = true,
		$autoDelete = false,
		$internal = false,
		$noWait = false,
		null, //$args,
		$ticket = null);
	$channel->queue_bind($queueName, $exchangeName);
}

if ($useDeadLetter) {
	$channel->exchange_declare($deadLetterExchangeName,
		$type = 'direct',
		$passive = false,
		$durable = true,
		$autoDelete = false,
		$internal = false,
		$noWait = false,
		null, //$args,
		$ticket = null
		);

	$channel->queue_declare($deadLetterQueueName,
		$passive = false,
		$durable = true,
		$exclusive = false,
		$autoDelete = false,
		$noWait = false,
		$args = null,
		$ticket = null);
	$channel->queue_bind($deadLetterQueueName, $deadLetterExchangeName);
}

// get the message that we'll send
if ($input) {
	//	read from input, send to the queue
	$messageData = file_get_contents("php://stdin");
} else {
	$messageData = "Hello World (sent to '${queueName}'@'${queueServer}"; 
}

// publish to the queue
$index = 0;
while($index < $numberOfMessages)
{
	$index += 1;
	$theMessageData = $input ? $messageData : "${messageData}; message # ${index}";
	$msg = new AMQPMessage($theMessageData, array('delivery_mode' => 2));
	//$channel->basic_publish($msg, $exchangeName, $queueName /* actually the routing key */);
	if ($useExchange) {
		$channel->basic_publish($msg, $exchangeName);
	} else {
		$channel->basic_publish($msg, '', $queueName);
	}
}

echo " [x] Sent ${index} message(s) ${messageData} to {$queueName} @ {$queueServer}" . PHP_EOL;

// close the channel and connection

$channel->close();
$connection->close();

