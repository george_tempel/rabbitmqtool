Sample send/receive scripts to exercise rabbitmq connectivity and features.

Background: There are two scripts, each driven by a bunch of optional command line parameters. The scripts are send.php and receive.php, used to send and receive job messages, respectively.

You do not have to sequence the send and receive scripts in any particular order. You could elect to use the send script to pump a LOT of messages into a queue, then once they're all there run one or more receive scripts to drain the queue. Or you could run one or more receiver scripts that are ready and waiting for queue items, then use the send script to being sending jobs into the queue and have them handled directly by the receivers.



RABBITMQ INSTALLATION:

If you wish to have a local rabbitmq server installed, you can use the setup.sh script to install and configure your linux box for a local rabbit. If you don't wish to use a local rabbitmq then you can ignore this step.

To install rabbitmq you'll need superuser (sudo) access on your linux box. To perform the setup using the provided script, use the following command line:

sudo ./setup.sh


PHP REQUIREMENTS:

Your PHP installation must have the mysqli extensions installed. This is because the receive.php script has grown to optionally exercise mysql connections. To check if your PHP installation is configured for mysql, type the following command line:

php -i | grep mysql

and ensure that you see some output such as

/etc/php.d/30-mysqli.ini,
mysqli
mysqli.allow_local_infile => On => On
mysqli.allow_persistent => On => On
mysqli.default_host => no value => no value
mysqli.default_port => 3306 => 3306
mysqli.default_pw => no value => no value
mysqli.default_socket => /var/lib/mysql/mysql.sock => /var/lib/mysql/mysql.sock
mysqli.default_user => no value => no value
mysqli.max_links => Unlimited => Unlimited
mysqli.max_persistent => Unlimited => Unlimited
mysqli.reconnect => Off => Off
mysqli.rollback_on_cached_plink => Off => Off
API Extensions => mysql,mysqli,pdo_mysql

If you don't have output similar to that above, you'll need to enable the mysqli extension on your PHP installation. (This readme does not yet have instructions on how to do that.)




PROJECT INSTALLATION:

This little PHP project uses Composer to pull-in external dependencies, such as the PHP library we use to connect to rabbitmq. Load the third party item(s) via the following command line:

php composer.phar install







SENDING JOB MESSAGES:

The send.php script is run via command lines such as:

php send.php

In its most simple form, it will attempt to connect to a local rabbitmq instance and send a "Hello, World" message to a queue named "hello". There are a lot of command line options you can use to instruct the script to connect elsewhere, specify username/password for queue access, number of messages, etc:

-s rabbitmqServerName (localhost is the default)
-p rabbitmqPort (5672 is the default)
-u rabbitmqUsername (guest is the default)
-w rabbitmqPassword (guest is the default)
-q nameOfQueueToUse (hello is the default)
-n numberOfMessagesToSend (default is 1)
-i readMessageContentFromStdin (default is to simply send "Hello, World" to the queue)

example--send two "Hello, World" messages to the queue "MyFirstQueue" on the ssnj-queue01 development queue server:

php send.php -q MyFirstQueue -s ssnj-queue01 -n 2



RECEIVING JOB MESSAGES:

The receive.php script is run via command lines such as:

php receive.php

In its most simple form, it will attempt to connect to a local rabbitmq instance begin receiving messages. It supports other command line options to simulate negative-acknowledgments, errors, to requeue messages, to each message count, echo the message, etc.

-s rabbitmqServerName (localhost is the default)
-p rabbitmqPort (5672 is the default)
-u rabbitmqUsername (guest is the default)
-w rabbitmqPassword (guest is the default)
-q nameOfQueueToUse (hello is the default)
-e errorEveryNMessages (0 is the default)
-n negativeAcknowledgeEveryNMessages (0 is the default)
-c (if specified, echo the message count)
-v (if specified, output verbose information)
-r (if specified, as well as -n, it will requeue when negatively acknowledging)
-d (if specified, it will open/close a mysql connection...credentials are placeholders in the dbcredentials.php script, which you must edit)

example--receive, and echo, messages from the queue "MyFirstQueue" on the ssnj-queue01 development queue server:

php receive.php -q MyFirstQueue -s ssnj-queue01 -v 


The receive.php script must be terminated via a control-c interrupt, and will emit some information on the exception that killed the script. 

To run several receiver scripts, you should either run each in the background (by specifing a "&" at the end of the command line), or run separate scripts in separate terminal windows/tabs/sessions. For instance, to run several in the background use a command line such as

php receive.php -q MyFirstQueue -s ssnj-queue01 -v &
php receive.php -q MyFirstQueue -s ssnj-queue01 -v &
php receive.php -q MyFirstQueue -s ssnj-queue01 -v &

which will run three receivers in the background, each connecting to 'MyFirstQueue' on ssnj-queue01.





FUTURE WORK:

We could attach options for mysql database credentials
We could allow for a query in the database exercise
