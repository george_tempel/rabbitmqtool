<?php

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Connection\AMQPStreamConnection;

// the lib can use pcntl_XXX in various points, but if
// you wish to avoid that define the following:
//define('AMQP_WITHOUT_SIGNALS', true);

$options = getopt('q:s:p:u:w:de:n:vrc',
	array(
		'queue:',
		'server:',
		'port:',
		'username:',
		'password:',
	)
); 
$queueName = array_key_exists('q', $options) ? $options['q'] : (array_key_exists('queue', $options) ? $options['queue'] : 'hello');
$queueServer = array_key_exists('s', $options) ? $options['s'] : (array_key_exists('server', $options) ? $options['server'] : 'localhost');
$queuePort = array_key_exists('p', $options) ? $options['p'] : (array_key_exists('port', $options) ? $options['port'] : 5672);
$username = array_key_exists('u', $options) ? $options['u'] : (array_key_exists('username', $options) ? $options['username'] : 'guest');
$password = array_key_exists('w', $options) ? $options['w'] : (array_key_exists('password', $options) ? $options['password'] : 'guest');
$dbExercise = array_key_exists('d', $options);
$errorEvery = abs(1 * array_key_exists('e', $options) ? $options['e'] : 0);
$nackEvery = abs(1 * array_key_exists('n', $options) ? $options['n'] : 0);
$echoCount = array_key_exists('c', $options);
$verbose = array_key_exists('v', $options);
$requeue = array_key_exists('r', $options);

if ($dbExercise && !file_exists('dbcredentials.php')) {
	throw new Exception('Cannot use the database exercise switch without having the dbcredentials.php file');
}
$db = include_once('dbcredentials.php');

// set up the connection
$connection = new AMQPStreamConnection($queueServer, $queuePort, $username, $password);
$channel = $connection->channel();

/*
$deadLetterQueueName = $queueName . '.deadLetter.queue';
$deadLetterExchangeName = $queueName . '.deadLetter.exchange';
$args = [
        'x-dead-letter-exchange' => ['S', $deadLetterExchangeName],
        //'x-dead-letter-routing-key' => ['S', $queueName . '.deadLetter.routingKey' ],
];
*/
$args = [];
$channel->queue_declare($queueName,
$passive = false,
$durable = true,
$exclusive = false,
$autoDelete = false,
$noWait = false,
$args,
$ticket = null);

echo ' [*] Waiting for messages from ' . $queueName . '@' . $queueServer . '; to exit press CTRL+C' . PHP_EOL; 
if ($dbExercise) {
	error_log(' [*] Messages will exercise a db connection' . PHP_EOL);
}
if ($errorEvery > 0) {
	error_log(" [*] Will error/not-acknowledge 1 in every ${errorEvery} messages" . PHP_EOL);
}
if ($nackEvery > 0) {
	error_log(" [*] Will nack/negatively-acknowledge 1 in every ${nackEvery} messages" . PHP_EOL);
}

// define our callback that'll be used to consume messages
// coming off of the queue
$messageCount = 0;
$callback = function($msg) use (&$messageCount, $dbExercise, $db, $errorEvery, $nackEvery, $verbose, $echoCount, $requeue) {
	$dbi = null;
	if ($dbExercise) {
		try
		{
			$dbi = mysqli_init();
			$dbi->real_connect($db['host'], $db['username'], $db['password'], $db['name']);
			$dbi->close();
		}
		catch (Exception $e)
		{
			error_log("Exception handling db: ${e}");
			error_log($e->getTraceAsString());
		}
	}	

	$messageCount += 1;

	if ($verbose) {
		error_log(" [*] received message ${messageCount}: " . $msg->body . PHP_EOL);
	} else if ($echoCount) {
		error_log(" [*] received message ${messageCount}" . PHP_EOL);
	}

	if (($errorEvery > 0) && (($messageCount % $errorEvery) == 0)) {
		//	don't acknowledge
		error_log(" [*] not-acknowledging message ${messageCount} " . PHP_EOL);
	} else if (($nackEvery > 0) && (($messageCount % $nackEvery) == 0)) {
		error_log(" [*] nack'ing message ${messageCount} " . PHP_EOL);
		//$requeue = $msg->delivery_info['redelivered'] ? false : true;
		if ($requeue) {
			error_log(" [*] requeuing message ${messageCount} " . PHP_EOL);
		} else {
			error_log(" [*] dead-lettering message ${messageCount} " . PHP_EOL);
		}
			
		$msg->delivery_info['channel']->basic_nack($msg->delivery_info['delivery_tag'], $requeue);
	} else {
		if ($msg->delivery_info['redelivered']) {
			error_log(" [*] Message #${messageCount} is a redelivery: " . $msg->delivery_info['redelivered'] . PHP_EOL);
		}
		if ($verbose) {
			error_log(" [*] acknowledging message ${messageCount} " . PHP_EOL);
		}
		// acknowledge the message so it'll come off the queue
		$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
	}
};

// run flag is used to control if/when we bail due to signals, termination, etc
$run = true;
$signalHandler = function($signal) use (&$run) {
	echo "Caught signal ${signal}, will terminate shortly" . PHP_EOL;
	$run = false;
	echo "      setting run to " . ($run ? 'true' : 'false') . PHP_EOL;
};


if (extension_loaded('pcntl')) {
	// as often as possible, check the signal
	// 1 is quite often (and can be expensive); higher values (10, 100) are increasingly less often
	declare(ticks = 10) {
		foreach([\SIGHUP, \SIGINT, \SIGQUIT, \SIGTERM, \SIGUSR1] as $sigNum) {
			echo "handler for ${sigNum}" . PHP_EOL;
			pcntl_signal($sigNum, $signalHandler);
		}
	}
}


// used to allow periodic signal checking/handling
function checkSignals() {
	if (extension_loaded('pcntl')) {
		//echo "checking signals..." . PHP_EOL;
		pcntl_signal_dispatch();
	}
}


// rabbitmq just blindly dispatches every nth message to the nth consumer...we can
// defeat that with basic_qos and prefetch_count of 1, telling it not to give more
// than one message to a worker at a time. That is, don't dispatch a message to 
// a worker until it's processed and acknowledged completion of the previous one.
// The queue dispatcher will instead find the next available worker. KEEP IN MIND
// that queues can fill up if you're not careful.
$channel->basic_qos(null, 1, null);

// begin consuming messages
$channel->basic_consume($queueName, '', false, false, false, false, $callback);

// block while the channel has things for us to work on
try {
	while($run) {
		//echo "run: " . ($run ? 'true' : 'false') . PHP_EOL;

		// this if moved out of the while() condition because it can
		// block for some time, and cause issues with signal handling
		if (count($channel->callbacks)) {
			// the wait can be invoked with no parameters, and it's totally blocking
			// or it can be invoked with several parameters, including one indicating
			// blocking/non, and a timeout. If you specify the timeout, it'll throw
			// an exception at the end of the timeout, so be prepared to catch it.	
			// 
			try {
    				$channel->wait($allowedMethods = null, $nonBlocking = false, $timeout = 10);
    				//$channel->wait();
			} catch (\PhpAmqpLib\Exception\AMQPTimeoutException $timeoutException) {
				// ok
				echo "Caught timeout exception" . PHP_EOL;
			} catch (\PhpAmqpLib\Exception\AMQPRuntimeException $runtimeException) {
				// might be ok
				echo "Caught runtime exception" . PHP_EOL;
			} catch (Exception $e) {
				echo "Caught exception ${e}" . PHP_EOL;
			}
		}
		else
		{
			throw new Exception("No callbacks? There should be; this is likely an error.");
		}
		checkSignals();
	}
} catch (\PhpAmqpLib\Exception\AMQPRuntimeException $e) {
	echo "Terminating: runtime exception ${e}" . PHP_EOL;
} catch (\PhpAmqpLib\Exception\AMQPExceptionInterface $e) {
	echo "Terminating: exception ${e}" . PHP_EOL;
} catch (\Exception $e) {
	echo "Terminating due to exception: ${e}" . PHP_EOL;
}
echo "Finished.";
